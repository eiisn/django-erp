from django.template import loader


def get_default_data(request, module, add_form=None, update_form=None, columns=None, ajax_url=None, order=1, targets=None,
                     script=None,
                     template_table="generic/index_table.html",
                     template_form_add="generic/module_form_add.html",
                     initial_form_add=None,
                     template_form_update="generic/module_form_update.html",
                     initial_form_update=None,):
    """
    :param request: HttpRequest
    :param str module: ERP Module name to set elements id
    :param Form add_form: Django Form for adding module object
    :param Form update_form: Django Form for updating module object
    :param list columns: Columns show in the Datatable
    :param int order: Columns index to order Datatable
    :param str ajax_url: API url for GET/POST/DELETE/PUT
    :param list or int targets: Targets columns for boolean render
    :param None or bool or str script: If None no script, if True take module name script else script path
    :param str template_table: Change default generic table template
    :param str template_form_add: Change default generic add form template
    :param dict or None initial_form_add: args for add form
    :param str template_form_update: Change default generic update form template
    :param dict or None initial_form_update: args for add form
    :return: data
    :rtype: dict
    """
    if initial_form_update is None:
        initial_form_update = {}
    if initial_form_add is None:
        initial_form_add = {}
    if not columns:
        columns = [""]
    if not update_form:
        update_form = add_form
    form_add = None
    form_update = None
    if add_form:
        form_add = loader.render_to_string(template_form_add, {
            'module': module,
            'form': add_form(prefix="add", initial=initial_form_add),
        }, request)
    if update_form:
        form_update = loader.render_to_string(template_form_update, {
            'module': module,
            'form': update_form(prefix="update", initial=initial_form_update),
        }, request)
    data = {
        "module": module,
        "script": None if script is None else "/static/js/erp/modules/" + module + ".js" if script is True else script,
        "html": loader.render_to_string(template_table, {
            "module": module,
            "list_columns": columns
        }),
        "formAdd": form_add,
        "formUpdate": form_update,
        "options": {
            'ajax': ajax_url,
            'dom': 'Bfrtip',
            "select": {
                "style": 'os',
                "selector": 'td'
            },
            "order": [[order, 'asc']],
            'buttons': [
                {
                    'extend': 'print',
                    'className': "btn btn-secondary",
                    'text': '<i class="fas fa-print"></i>',
                    'exportOptions': {
                        'orthogonal': 'export'
                    }
                },
                {
                    'extend': 'pdf',
                    'className': 'btn btn-secondary',
                    'text': '<i class="fas fa-file-pdf"></i>',
                    'exportOptions': {
                        'orthogonal': 'export'
                    }
                },
                {
                    'text': '<i class="fas fa-sync"></i>',
                    'className': 'btn btn-secondary',
                    'attr': {'id': '%s-btn-sync' % module},
                    'action': True
                }
            ],
            'columnDefs': [
                {
                    "render": "",
                    "targets": targets
                },
                {
                    "targets": 0,
                    "data": None,
                    "defaultContent": '',
                    "orderable": False,
                    "className": 'select-checkbox'
                }
            ],
            "columns": [
                {"data": None},
            ]
        }
    }
    return data
