"""django_erp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from erp.views import main


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('accounts/api/', include('accounts.urls_api')),
    path('customer/', include('customer.urls')),
    path('customer/api/', include('customer.urls_api')),
    path('product/', include('product.urls')),
    path('product/api/', include('product.urls_api')),
    path('inventory-stock/', include('inventory_stock.urls')),
    path('inventory-stock/api/', include('inventory_stock.urls_api')),
    path('purchase/', include('purchase.urls')),
    path('purchase/api/', include('purchase.urls_api')),
    path('sale/', include('sale.urls')),
    path('sale/api/', include('sale.urls_api')),
    path('manufacturing/', include('manufacturing.urls')),
    path('manufacturing/api/', include('manufacturing.urls_api')),
    path('address/', include('erp.urls')),
    path('address/api/', include('erp.urls_api')),
    path('', main, name='index'),
]
