from django.contrib import admin
from erp.models import *


# Register your models here.
admin.site.register(Address)
admin.site.register(Notification)
admin.site.register(InvoiceState)
