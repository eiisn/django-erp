from django.db import models


class InvoiceState(models.Model):
    libel = models.CharField(max_length=255)

    def __str__(self):
        return self.libel
