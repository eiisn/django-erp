from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Notification(models.Model):
    WARNING = ("W", "warning")
    DANGER = ("D", "danger")
    SUCCESS = ("S", "success")
    INFO = ("I", "info")
    SEVERITY = (
        (WARNING[0], WARNING[1]),
        (DANGER[0], DANGER[1]),
        (SUCCESS[0], SUCCESS[1]),
        (INFO[0], INFO[1])
    )
    title = models.CharField(max_length=64)
    content = models.TextField(max_length=255)
    severity = models.CharField(max_length=64, choices=SEVERITY)
    closed = models.BooleanField(default=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.title
