from erp.models import Address
from utils.ERPView import ERPView
from erp.serializers import AddressGETSerializer, AddressPOSTSerializer


class AddressView(ERPView):

    model = Address
    post_serializer = AddressPOSTSerializer
    get_serializer = AddressGETSerializer
