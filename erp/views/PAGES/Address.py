from utils import get_default_data
from erp.forms.Address import AddressForm
from django.http import JsonResponse, HttpResponseForbidden


def address_index(request):
    if request.is_ajax():
        columns = ["", "Street", "Zip", "City", "Country", "Building Name", "Active", "Invoice", "Delivery",
                   "Longitude", "Latitude"]
        data = get_default_data(request, "address", AddressForm, None, columns, "address/api/address/", 1, [6, 7, 8])
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'street'},
            {'data': 'zip'},
            {'data': 'city'},
            {'data': 'country'},
            {'data': 'building_name'},
            {'data': 'active'},
            {'data': 'invoice'},
            {'data': 'delivery'},
            {'data': 'longitude'},
            {'data': 'latitude'}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
