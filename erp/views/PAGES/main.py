from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import render, resolve_url

from erp.models import Notification
from product.models import Product


def main(request):
    side_link = {
        "customer": {
            "name": "Customer",
            "icon": "user",
            "modules": {
                "customer": ["Customer", resolve_url('customer-index'), 'user'],
                "address": ["Address", resolve_url('address-index'), 'map']
            }
        },
        "product": {
            "name": "Product",
            "icon": "cubes",
            "modules": {
                "product": ["Product", resolve_url('product-index'), 'cubes'],
                "unit-of-measure": ["Unit of Measure", resolve_url('unit-of-measure-index'), 'square-root-alt'],
                "product-type": ["Product Type", resolve_url('product-type-index'), 'caret-right'],
                "account-category": ["Account Category", resolve_url('account-category-index'), 'landmark']
            }
        },
        "inventory-stock": {
            "name": "Inventory & Stock",
            "icon": "warehouse",
            "modules": {
                "location": ["Location", resolve_url('location-index'), 'map-marker'],
                "inventory": ["Inventory", resolve_url('inventory-index'), 'dolly'],
                "shipment": ["Shipment", resolve_url('shipment-index'), 'truck-moving'],
            }
        },
        "purchase": {
            "name": "Purchasing",
            "icon": "store",
            "modules": {
                "purchase": ["Purchasing", resolve_url('purchase-index'), 'caret-right']
            }
        },
        "sale": {
            "name": "Sales",
            "icon": "university",
            "modules": {
                "sale": ["Sales", resolve_url('sale-index'), 'caret-right'],
            }
        },
        "manufacturing": {
            "name": "Manufacturing",
            "icon": "industry",
            "modules": {
                "raw-matter": ["Raw Matter", resolve_url('raw-matter-index'), 'seedling'],
                "formula": ["Formula", resolve_url('formula-index'), 'vial'],
                "scale": ["Scale", resolve_url('scale-index'), 'clipboard-list'],
                "nomenclature": ["Nomenclature", resolve_url('nomenclature-index'), 'receipt'],
                "production": ["Production", resolve_url('production-index'), 'caret-right'],
                "quality": ["Quality", resolve_url('quality-index'), 'check-circle']
            }
        }
    }

    return render(request, "index.html", {
        "sidebar": side_link,
        "notifications": Notification.objects.filter(closed=False)
    })


def close_notif(request, pk_notif):
    if request.is_ajax():
        notification = Notification.objects.get(pk=pk_notif)
        notification.closed = True
        notification.save()
        return JsonResponse({"data": "OK"})
    return HttpResponseForbidden()
