from django import forms
from erp.models import Address


class AddressForm(forms.ModelForm):

    class Meta:
        model = Address
        exclude = ("id", "latitude", "longitude")
