"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from inventory_stock.views.API import *


urlpatterns = [
    path('inventory/', InventoryView.as_view()),
    path('inventory/<int:pk>/', InventoryView.as_view()),
    path('inventory-line/<int:pk_inventory>/', InventoryLineView.as_view()),
    path('inventory-line/<int:pk_inventory>/<int:pk>/', InventoryLineView.as_view()),
    path('location/', LocationView.as_view()),
    path('location/<int:pk>/', LocationView.as_view()),
    path('shipment/', ShipmentView.as_view()),
    path('shipment/<int:pk>/', ShipmentView.as_view()),
]
