from rest_framework import serializers
from inventory_stock.models import Shipment


class ShipmentGETSerializer(serializers.ModelSerializer):
    delivery_address = serializers.StringRelatedField()

    class Meta:
        model = Shipment
        fields = '__all__'
        depth = 2


class ShipmentPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shipment
        fields = '__all__'
