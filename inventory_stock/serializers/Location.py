from rest_framework import serializers

from inventory_stock.models import Location, LocationType


class LocationGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'
        depth = 1


class LocationPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'

    def create(self, validated_data):
        new_location = validated_data.pop('new_location', None)
        if new_location and not validated_data['location_type']:
            validated_data['location_type'] = LocationType.objects.get_or_create(libel=new_location)[0]
        return Location.objects.create(**validated_data)

    def to_internal_value(self, data):
        internal_value = super(LocationPOSTSerializer, self).to_internal_value(data)
        new_location = data.get("new_location")
        if new_location:
            internal_value.update({
                "new_location": new_location
            })
        return internal_value
