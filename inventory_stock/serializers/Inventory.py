from rest_framework import serializers

from inventory_stock.models import Inventory, InventoryLine


class InventoryGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Inventory
        fields = '__all__'
        depth = 2


class InventoryPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Inventory
        fields = '__all__'


class InventoryLineGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = InventoryLine
        fields = '__all__'
        depth = 2


class InventoryLinePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = InventoryLine
        fields = '__all__'

    def __init__(self, inventory=None, *args, **kwargs):
        super(InventoryLinePOSTSerializer, self).__init__(*args, **kwargs)
        self.__inventory = inventory

    def create(self, validated_data):
        validated_data["inventory"] = Inventory.objects.get(pk=self.__inventory)
        return InventoryLine.objects.create(**validated_data)
