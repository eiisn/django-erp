from django.contrib import admin
from inventory_stock.models import *

# Register your models here.
admin.site.register(Inventory)
admin.site.register(InventoryLine)
admin.site.register(Location)
admin.site.register(LocationType)
admin.site.register(Shipment)
admin.site.register(ShipmentState)
