from utils.ERPView import ERPView
from inventory_stock.models import Shipment
from inventory_stock.serializers import ShipmentGETSerializer, ShipmentPOSTSerializer


class ShipmentView(ERPView):

    model = Shipment
    post_serializer = ShipmentPOSTSerializer
    get_serializer = ShipmentGETSerializer
