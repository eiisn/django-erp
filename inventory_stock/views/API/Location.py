from utils.ERPView import ERPView
from inventory_stock.models import Location
from inventory_stock.serializers import LocationGETSerializer, LocationPOSTSerializer


class LocationView(ERPView):

    model = Location
    post_serializer = LocationPOSTSerializer
    get_serializer = LocationGETSerializer
