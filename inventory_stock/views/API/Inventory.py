from rest_framework import status
from rest_framework.response import Response
from inventory_stock.models import Inventory, InventoryLine
from inventory_stock.serializers import InventoryGETSerializer, InventoryPOSTSerializer, InventoryLineGETSerializer, \
    InventoryLinePOSTSerializer
from utils.ERPView import ERPView


class InventoryView(ERPView):

    model = Inventory
    post_serializer = InventoryPOSTSerializer
    get_serializer = InventoryGETSerializer


class InventoryLineView(ERPView):

    model = InventoryLine
    post_serializer = InventoryLinePOSTSerializer
    get_serializer = InventoryLineGETSerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                inventory_line = InventoryLine.objects.get(pk=pk)
                serialize_data = InventoryLineGETSerializer(inventory_line)
            except InventoryLine.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            pk_inventory = kwargs.get('pk_inventory', None)
            if pk_inventory:
                inventory_line = InventoryLine.objects.filter(inventory__pk=pk_inventory)
                serialize_data = InventoryLineGETSerializer(inventory_line, many=True)
            else:
                return Response({"data": "Need pk_inventory"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        pk_inventory = kwargs.get('pk_inventory', None)
        if pk_inventory:
            data = {"inventory": pk_inventory}
            serializer = InventoryLinePOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_inventory"}, status=status.HTTP_404_NOT_FOUND)
