from utils import get_default_data
from inventory_stock.forms.Shipment import ShipmentForm
from django.http import JsonResponse, HttpResponseForbidden


def shipment_index(request):
    if request.is_ajax():
        columns = ["", "Reference", "Planned Date", "Effective Date", "Customer", "Delivery Address", "State", "Return"]
        data = get_default_data(request, "shipment", ShipmentForm, None, columns, "inventory-stock/api/shipment/",
                                1, [7])
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'reference'},
            {'data': 'planned_date'},
            {'data': 'effective_date'},
            {'data': 'customer.name'},
            {'data': 'delivery_address'},
            {'data': 'state.libel'},
            {'data': 'return_shipment'},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
