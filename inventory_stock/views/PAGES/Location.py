from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden
from inventory_stock.forms.Location import LocationForm


def location_index(request):
    if request.is_ajax():
        columns = ["", "Code", "Name", "Type"]
        data = get_default_data(request, "location", LocationForm, None, columns, "inventory-stock/api/location/")
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'code'},
            {'data': 'name'},
            {'data': 'location_type.libel'}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
