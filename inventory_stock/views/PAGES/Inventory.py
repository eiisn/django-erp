from inventory_stock.forms.Inventory import InventoryForm, InventoryLineForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def inventory_index(request):
    if request.is_ajax():
        columns = ["", "", "Location", "Company", "Date"]
        data = get_default_data(request, "inventory", InventoryForm, None, columns, "inventory-stock/api/inventory/", 2,
                                script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {'data': 'location.name'},
            {'data': 'company.name'},
            {'data': 'date'}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def inventory_line_index(request, pk_inventory):
    if request.is_ajax():
        columns = ["", "Product", "Quantity", "Expected Quantity", "Unit of Measure"]
        data = get_default_data(request, "inventory-line-%s" % pk_inventory, InventoryLineForm, None, columns,
                                "inventory-stock/api/inventory-line/%s/" % pk_inventory, 1,
                                initial_form_add={"inventory": pk_inventory})
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'product.name'},
            {'data': 'quantity'},
            {'data': 'expected_quantity'},
            {'data': 'unit_of_measure.name'}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
