from django.db import models


class Location(models.Model):
    code = models.CharField(max_length=64)
    name = models.CharField(max_length=255)
    location_type = models.ForeignKey("LocationType", models.SET_NULL, null=True)
    address = models.ForeignKey("erp.Address", models.SET_NULL, null=True)
    factory = models.BooleanField(default=False)
    warehouse = models.BooleanField(default=False)

    def __str__(self):
        return "%s [%s]" % (self.name, self.code)


class LocationType(models.Model):
    libel = models.CharField(max_length=255)

    def __str__(self):
        return self.libel
