from django.db import models


class Inventory(models.Model):
    date = models.DateField(blank=True, null=True)
    location = models.ForeignKey("Location", models.SET_NULL, null=True)
    company = models.ForeignKey("customer.Customer", models.SET_NULL, null=True)

    class Meta:
        verbose_name = "Inventory"
        verbose_name_plural = "Inventories"

    def __str__(self):
        return "%s - %s" % (self.location.name, self.date)


class InventoryLine(models.Model):
    inventory = models.ForeignKey("Inventory", models.SET_NULL, null=True)
    product = models.ForeignKey("product.Product", models.CASCADE)
    expected_quantity = models.IntegerField(default=0)
    quantity = models.IntegerField(default=0)
    unit_of_measure = models.ForeignKey("product.UnitOfMeasure", models.SET_NULL, null=True)

    class Meta:
        verbose_name = "Inventory line"
        verbose_name_plural = "Inventories lines"

    def __str__(self):
        return "%s - %s/%s%s" % (self.product.name, self.quantity, self.expected_quantity, self.unit_of_measure.symbol)
