from django.db import models


class Shipment(models.Model):
    reference = models.CharField(max_length=255, unique=True)
    return_shipment = models.BooleanField(default=False)
    planned_date = models.DateField(null=True, blank=True)
    effective_date = models.DateField(null=True, blank=True)
    customer = models.ForeignKey("customer.Customer", models.SET_NULL, null=True)
    delivery_address = models.ForeignKey("erp.Address", models.SET_NULL, null=True)
    start_location = models.ForeignKey("Location", models.SET_NULL, null=True)
    state = models.ForeignKey("ShipmentState", models.SET_NULL, null=True)

    def __str__(self):
        return "%s %s %s %s" % ("Return" if self.return_shipment else "To",
                                self.customer.name, self.delivery_address, self.state)

    def address(self):
        return self.delivery_address.address


class ShipmentState(models.Model):
    libel = models.CharField(max_length=255)

    def __str__(self):
        return self.libel
