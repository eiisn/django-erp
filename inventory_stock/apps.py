from django.apps import AppConfig


class InventoryStockConfig(AppConfig):
    name = 'inventory_stock'
