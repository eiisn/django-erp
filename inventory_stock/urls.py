"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from inventory_stock.views import *


urlpatterns = [
    path('get-inventory-index/', inventory_index, name='inventory-index'),
    path('get-inventory-line-index/<int:pk_inventory>/', inventory_line_index, name='inventory-line-index'),
    path('get-location-index/', location_index, name='location-index'),
    path('get-shipment-index/', shipment_index, name='shipment-index'),
]
