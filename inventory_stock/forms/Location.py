from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, Button
from django import forms

from inventory_stock.models import Location


class LocationForm(forms.ModelForm):
    new_location = forms.CharField(max_length=64, required=False)

    class Meta:
        model = Location
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.fields['location_type'].required = False
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name'),
            Field('code'),
            Div(
               Div(Field('location_type'), css_class="col"),
               Div(Button("Ou", "Ou", disabled="ok"), css_class="col"),
               Div(Field('new_location'), css_class="col"),
               css_class="row"
            ),
            Field('address'),
            Field('factory'),
            Field('warehouse')
        )
