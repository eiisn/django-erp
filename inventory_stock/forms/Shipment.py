from django import forms

from inventory_stock.models import Shipment


class ShipmentForm(forms.ModelForm):
    planned_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )
    effective_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )

    class Meta:
        model = Shipment
        exclude = ('id',)
