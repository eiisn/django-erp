from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from inventory_stock.models import Inventory, InventoryLine


class InventoryForm(forms.ModelForm):
    date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            },
        )
    )

    class Meta:
        model = Inventory
        exclude = ('id',)


class InventoryLineForm(forms.ModelForm):

    class Meta:
        model = InventoryLine
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(InventoryLineForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('inventory', disabled=True),
            Field('product'),
            Field('quantity'),
            Field('expected_quantity'),
            Field('unit_of_measure')
        )
