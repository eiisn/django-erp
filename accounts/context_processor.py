from django.contrib.auth.forms import AuthenticationForm


def login_form(request):
    context = {"login_form": None}
    if not request.user.is_authenticated:
        context["login_form"] = AuthenticationForm()
    return context
