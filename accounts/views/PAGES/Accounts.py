from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import JsonResponse


def ajax_login(request):
    response_data = {}
    try:
        if request.POST and request.is_ajax():
            username = request.POST["username"]
            password = request.POST["password"]
            try:
                get_user = User.objects.get(username=username)
                if get_user.check_password(password):
                    user = authenticate(username=username, password=password)
                    if user is not None:
                        login(request, user)
                        response_data = {"login": "Success"}
                else:
                    response_data = {"user": "wrong password"}
            except User.DoesNotExist:
                response_data = {"user": "no user"}
        else:
            response_data = {"login": "Failed"}
    except Exception as e:
        print("Error: %s" % str(e))
        response_data = {"login": str(e)}
    return JsonResponse(response_data)
