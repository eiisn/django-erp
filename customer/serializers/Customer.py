from rest_framework import serializers

from customer.models import Customer


class CustomerGETSerializer(serializers.ModelSerializer):

    address = serializers.StringRelatedField()

    class Meta:
        model = Customer
        fields = '__all__'


class CustomerPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = '__all__'
