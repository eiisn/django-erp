from utils.ERPView import ERPView
from customer.models import Customer
from customer.serializers import CustomerGETSerializer, CustomerPOSTSerializer


class CustomerView(ERPView):

    model = Customer
    post_serializer = CustomerPOSTSerializer
    get_serializer = CustomerGETSerializer
