from utils import get_default_data
from customer.forms.Customer import CustomerForm
from django.http import JsonResponse, HttpResponseForbidden


def customer_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Language", "Address", "Tax Identifier", "Receivable Today", "Payable Today"]
        data = get_default_data(request, "customer", CustomerForm, None, columns, "customer/api/customer/")
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'name'},
            {'data': 'language'},
            {'data': 'address'},
            {'data': 'tax_identifier'},
            {'data': 'receivable_today'},
            {'data': 'payable_today'},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
