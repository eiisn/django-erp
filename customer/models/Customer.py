from django.db import models
from django.conf.global_settings import LANGUAGES


class Customer(models.Model):
    name = models.CharField(max_length=255)
    language = models.CharField(max_length=64, choices=LANGUAGES)
    tax_identifier = models.IntegerField()
    receivable_today = models.DecimalField(max_digits=11, decimal_places=2)
    payable_today = models.DecimalField(max_digits=11, decimal_places=2)
    address = models.ForeignKey("erp.Address", models.SET_NULL, null=True)

    class Meta:
        verbose_name_plural = "Customers"
        verbose_name = "Customer"

    def __str__(self):
        return self.name
