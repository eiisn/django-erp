from django import forms
from product.models import Product, RawMatter, Formula, Scale, Step, Nomenclature
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['product_type'].required = False
        self.fields['account_category'].required = False
        self.fields['unit_of_measure'].required = False
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name'),
            Field('list_price'),
            Field('cost_price'),
            Field('product_type'),
            Field('account_category'),
            Field('unit_of_measure'),
            Div(Field('salable', css_class='form-check-input'), css_class="form-check form-check-inline"),
            Div(Field('purchasable', css_class='form-check-input'), css_class="form-check form-check-inline"),
            Div(Field('producible', css_class='form-check-input'), css_class="form-check form-check-inline")
        )


class RawMatterForm(forms.ModelForm):

    class Meta:
        model = RawMatter
        exclude = ('id',)


class FormulaForm(forms.ModelForm):

    molecule = forms.ModelMultipleChoiceField(RawMatter.objects.all())

    class Meta:
        model = Formula
        exclude = ('id',)


class ScaleForm(forms.ModelForm):

    class Meta:
        model = Scale
        exclude = ('id',)


class StepForm(forms.ModelForm):

    class Meta:
        model = Step
        exclude = ('id',)


class NomenclatureForm(forms.ModelForm):

    class Meta:
        model = Nomenclature
        exclude = ('id',)
