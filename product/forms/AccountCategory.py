from django import forms
from product.models import AccountCategory


class AccountCategoryForm(forms.ModelForm):

    class Meta:
        model = AccountCategory
        fields = ["name"]
