from django import forms
from product.models import UnitOfMeasure
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, Button


class UnitOfMeasureForm(forms.ModelForm):
    new_category = forms.CharField(max_length=64, required=False)

    class Meta:
        model = UnitOfMeasure
        fields = ["name", "symbol", "category", "factor", "rate", "rounding_precision"]

    def __init__(self, *args, **kwargs):
        super(UnitOfMeasureForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = False
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name'),
            Field('symbol'),
            Div(
                Div(Field('category'), css_class="col"),
                Div(Button("Ou", "Ou", disabled="ok"), css_class="col"),
                Div(Field('new_category'), css_class="col"),
                css_class="row"
            ),
            Field('factor'),
            Field('rate'),
            Field('rounding_precision'),
        )
