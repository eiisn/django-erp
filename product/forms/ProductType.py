from django import forms
from product.models import ProductType


class ProductTypeForm(forms.ModelForm):

    class Meta:
        model = ProductType
        fields = ['name']
