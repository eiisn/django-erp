from utils.ERPView import ERPView
from product.models import AccountCategory
from product.serializers.AccountCategory import AccountCategorySerializer


class AccountCategoryView(ERPView):

    model = AccountCategory
    post_serializer = AccountCategorySerializer
    get_serializer = AccountCategorySerializer
