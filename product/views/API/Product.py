from django.contrib.contenttypes.models import ContentType
from rest_framework.views import APIView

from erp.models import Notification
from utils.ERPView import ERPView
from rest_framework import status, authentication, permissions
from rest_framework.response import Response
from product.models import Product, Formula, RawMatter, Scale, Step, Nomenclature
from product.serializers import ProductGETSerializer, ProductPOSTSerializer, FormulaPOSTSerializer, \
    FormulaGETSerializer, RawMatterPOSTSerializer, RawMatterGETSerializer, SalableProductGETSerializer, \
    ScalePOSTSerializer, ScaleGETSerializer, StepPOSTSerializer, StepGETSerializer, NomenclaturePOSTSerializer, \
    NomenclatureGETSerializer


class ProductView(ERPView):

    model = Product
    post_serializer = ProductPOSTSerializer
    get_serializer = ProductGETSerializer


class SalableProductView(APIView):

    http_method_names = ["get"]
    authentication_classes = [authentication.TokenAuthentication, authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        products = Product.objects.filter(salable=True)
        serializer_product = SalableProductGETSerializer(products, many=True)
        return Response({"data": serializer_product.data}, status=status.HTTP_200_OK)


class RawMatterView(ERPView):

    model = RawMatter
    post_serializer = RawMatterPOSTSerializer
    get_serializer = RawMatterGETSerializer


class FormulaView(ERPView):

    model = Formula
    post_serializer = FormulaPOSTSerializer
    get_serializer = FormulaGETSerializer


class MoleculeView(RawMatterView):

    sub = 'molecule'
    parent = 'formula'
    parent_model = Formula


class ScaleView(ERPView):

    model = Scale
    post_serializer = ScalePOSTSerializer
    get_serializer = ScaleGETSerializer


class StepView(ERPView):

    model = Step
    post_serializer = StepPOSTSerializer
    get_serializer = StepGETSerializer
    sub = 'step'
    parent = 'scale'
    parent_model = Scale

    def post(self, request, *args, **kwargs):
        pk_scale = kwargs.get('pk_scale', None)
        if pk_scale:
            data = {"scale": pk_scale}
            serializer = StepPOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_purchase"}, status=status.HTTP_404_NOT_FOUND)


class NomenclatureView(ERPView):

    model = Nomenclature
    post_serializer = NomenclaturePOSTSerializer
    get_serializer = NomenclatureGETSerializer


class IngredientView(RawMatterView):

    sub = 'ingredient'
    parent = 'nomenclature'
    parent_model = Nomenclature


class AddProductView(APIView):

    http_method_names = ["post"]
    authentication_classes = [authentication.TokenAuthentication, authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            new_product = Product.objects.create(name=request.data["product"]["name"])

            raw_materials = []
            for mol in request.data["formula"]["molecule"]:
                raw_mat, created = RawMatter.objects.get_or_create(name=mol["name"])
                if created:
                    Notification.objects.create(**{
                        "title": "Nouvelle matière première nécessaire %s" % raw_mat,
                        "content": "Veuillez sélectionner un fournisseur pour ce matériaux",
                        "severity": Notification.WARNING[0],
                        "content_type": ContentType.objects.get(model=raw_mat._meta.model_name),
                        "object_id": raw_mat.id
                    })
                raw_materials.append(raw_mat)

            new_formula = Formula.objects.create(name=request.data["formula"]["name"])
            for raw_mat in raw_materials:
                new_formula.molecule.add(raw_mat)

            ingredients = []
            for ingredient in request.data["nomenclature"]["ingredient"]:
                new_ingredient, created = RawMatter.objects.get_or_create(name=ingredient["name"])
                if created:
                    Notification.objects.create(**{
                        "title": "Nouvel ingrédient est nécessaire %s" % new_ingredient,
                        "content": "Veuillez sélectionner un fournisseur pour cet ingrédient",
                        "severity": Notification.WARNING[0],
                        "content_type": ContentType.objects.get(model=new_ingredient._meta.model_name),
                        "object_id": new_ingredient.id
                    })
                ingredients.append(new_ingredient)

            new_nomenclature = Nomenclature.objects.create(name=request.data["nomenclature"]["name"],
                                                           product=new_product, formula=new_formula)
            for ingredient in ingredients:
                new_nomenclature.ingredient.add(ingredient)

            new_scale = Scale.objects.create(product=new_product, formula=new_formula)

            for step in request.data["scale"]["step"]:
                ingredient, created = RawMatter.objects.get_or_create(name=step["ingredient"]["name"])
                new_step = Step.objects.create(**{
                    "scale": new_scale,
                    "ingredient": ingredient,
                    "quantity": step["quantity"],
                    "order": step["order"],
                    "description": step["description"]
                })
                new_step.save()
            return Response({"data": "OK"}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"data": "KO retry later", "error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
