from utils.ERPView import ERPView
from product.models import ProductType
from product.serializers import ProductTypeSerializer


class ProductTypeView(ERPView):

    model = ProductType
    post_serializer = ProductTypeSerializer
    get_serializer = ProductTypeSerializer
