from utils.ERPView import ERPView
from product.models import UnitOfMeasure
from product.serializers import UnitOfMeasurePOSTSerializer, UnitOfMeasureGETSerializer


class UnitOfMeasureView(ERPView):

    model = UnitOfMeasure
    post_serializer = UnitOfMeasurePOSTSerializer
    get_serializer = UnitOfMeasureGETSerializer
