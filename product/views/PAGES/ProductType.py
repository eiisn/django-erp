from utils import get_default_data
from product.forms.ProductType import ProductTypeForm
from django.http import JsonResponse, HttpResponseForbidden


def product_type_index(request):
    if request.is_ajax():
        columns = ["", "Name"]
        data = get_default_data(request, "product-type", ProductTypeForm, None, columns, "product/api/product-type/")
        data["options"]["columns"] = data["options"]["columns"] + [{
            "data": "name"
        }]
        return JsonResponse(data)
    return HttpResponseForbidden()
