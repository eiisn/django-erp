from utils import get_default_data
from product.forms.Product import ProductForm, FormulaForm, RawMatterForm, ScaleForm, StepForm, NomenclatureForm
from django.http import JsonResponse, HttpResponseForbidden


def product_index(request):
    if request.is_ajax():
        columns = ["", "Name", "List Price", "Cost Price", "Type", "Account Cat.", "Unit of measure",
                   "Salable", "Purchasable", "Producible"]
        data = get_default_data(request, "product", ProductForm, None, columns, "product/api/product/", 1, [7, 8, 9])
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "name"},
            {"data": "list_price"},
            {"data": "cost_price"},
            {"data": "product_type.name", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "account_category.name", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "unit_of_measure.name", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "salable"},
            {"data": "purchasable"},
            {"data": "producible"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def raw_matter_index(request):
    if request.is_ajax():
        columns = ["", "Product", "Supplier"]
        data = get_default_data(request, "raw-matter", RawMatterForm, None, columns, "product/api/raw-matter/", 1)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "supplier.name"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def formula_index(request):
    if request.is_ajax():
        columns = ["", "", "Name"]
        data = get_default_data(request, "formula", FormulaForm, None, columns, "product/api/formula/", 2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "name"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def molecule_index(request, pk_formula):
    if request.is_ajax():
        columns = ["", "Product", "Supplier"]
        data = get_default_data(request, "molecule-formula-%s" % pk_formula, RawMatterForm, None, columns,
                                "product/api/molecule/%s/" % pk_formula, 1,)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "supplier.name"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def scale_index(request):
    if request.is_ajax():
        columns = ["", "", "Product", "Formula"]
        data = get_default_data(request, "scale", ScaleForm, None, columns, "product/api/scale/", 2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "product.name"},
            {"data": "formula.name"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def step_index(request, pk_scale):
    if request.is_ajax():
        columns = ["", "Ingredient", "Quantity", "Order", "Description"]
        data = get_default_data(request, "step-scale-%s" % pk_scale, StepForm, None, columns,
                                "product/api/step/%s/" % pk_scale, 3, initial_form_add={"scale": pk_scale})
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "ingredient.product.name"},
            {"data": "quantity"},
            {"data": "order"},
            {"data": "description"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def nomenclature_index(request):
    if request.is_ajax():
        columns = ["", "", "Name", "Product", "Formula"]
        data = get_default_data(request, "nomenclature", NomenclatureForm, None, columns, "product/api/nomenclature/",
                                2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "name"},
            {"data": "product.name"},
            {"data": "formula.name"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def ingredient_index(request, pk_nomenclature):
    if request.is_ajax():
        columns = ["", "Product", "Supplier"]
        data = get_default_data(request, "ingredient-nomenclature-%s" % pk_nomenclature, RawMatterForm, None, columns,
                                "product/api/ingredient/%s/" % pk_nomenclature, 1)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "supplier.name"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
