from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden
from product.forms.AccountCategory import AccountCategoryForm


def account_category_index(request):
    if request.is_ajax():
        columns = ["", "Name"]
        data = get_default_data(request, "account-category", AccountCategoryForm, None, columns,
                                "product/api/account-category/")
        data["options"]["columns"] = data["options"]["columns"] + [{
            "data": "name"
        }]
        return JsonResponse(data)
    return HttpResponseForbidden()
