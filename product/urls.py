"""product URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from product.views.PAGES import *


urlpatterns = [
    path('get-product-index/', product_index, name="product-index"),
    path('get-raw-matter-index', raw_matter_index, name="raw-matter-index"),
    path('get-formula-index/', formula_index, name="formula-index"),
    path('get-molecule-index/<int:pk_formula>/', molecule_index, name="molecule-index"),
    path('get-scale-index/', scale_index, name="scale-index"),
    path('get-step-index/<int:pk_scale>/', step_index, name="step-index"),
    path('get-nomenclature-index/', nomenclature_index, name="nomenclature-index"),
    path('get-ingredient-index/<pk_nomenclature>/', ingredient_index, name="ingredient-index"),
    path('get-account-category-index/', account_category_index, name='account-category-index'),
    path('get-product-type-index/', product_type_index, name='product-type-index'),
    path('get-unit-of-measure-index/', unit_of_measure_index, name='unit-of-measure-index'),
]
