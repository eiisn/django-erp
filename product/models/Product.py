from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255)
    list_price = models.DecimalField(max_digits=11, decimal_places=2, default=0, null=True)
    cost_price = models.DecimalField(max_digits=11, decimal_places=2, default=0, null=True)
    product_type = models.ForeignKey("product.ProductType", models.SET_NULL, null=True)
    account_category = models.ForeignKey("product.AccountCategory", models.SET_NULL, null=True)
    unit_of_measure = models.ForeignKey("product.UnitOfMeasure", models.SET_NULL, null=True)
    salable = models.BooleanField(default=False)
    purchasable = models.BooleanField(default=False)
    producible = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class RawMatter(models.Model):
    product = models.ForeignKey("Product", models.CASCADE)
    supplier = models.ForeignKey("customer.Customer", models.SET_NULL, null=True)

    def __str__(self):
        return self.product.name


class Formula(models.Model):
    name = models.CharField(max_length=64)
    molecule = models.ManyToManyField("RawMatter")

    def __str__(self):
        return "%s" % ("".join(list(self.molecule.all().values_list("product__name", flat=True))))


class Scale(models.Model):
    product = models.ForeignKey("Product", models.CASCADE)
    formula = models.ForeignKey("Formula", models.SET_NULL, null=True)

    def __str__(self):
        return "%s - %s" % (self.formula, self.product)


class Step(models.Model):
    scale = models.ForeignKey("Scale", models.CASCADE)
    ingredient = models.ForeignKey("RawMatter", models.CASCADE)
    quantity = models.DecimalField(max_digits=64, decimal_places=32, default=0)
    order = models.IntegerField(default=0)
    description = models.CharField(max_length=1024)

    def __str__(self):
        return "[%s][%s] %s x%s" % (self.scale, self.order, self.ingredient, self.quantity.normalize())


class Nomenclature(models.Model):
    name = models.CharField(max_length=64, blank=True, default="")
    ingredient = models.ManyToManyField("RawMatter")
    product = models.ForeignKey("Product", models.CASCADE)
    formula = models.ForeignKey("Formula", models.CASCADE)

    def __str__(self):
        return "[%s] %s" % (self.product.name, self.name)
