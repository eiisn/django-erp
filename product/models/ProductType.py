from django.db import models


class ProductType(models.Model):
    name = models.CharField(max_length=255)
    enable = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    disabled = models.DateTimeField(default=None, null=True, editable=True)

    def __str__(self):
        return self.name
