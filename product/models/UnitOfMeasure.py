from django.db import models


class UnitOfMeasure(models.Model):
    name = models.CharField(max_length=255)
    symbol = models.CharField(max_length=64)
    category = models.ForeignKey("UOMCategory", models.SET_NULL, 'uom_category', null=True)
    factor = models.DecimalField(max_digits=17, decimal_places=11)
    rate = models.DecimalField(max_digits=17, decimal_places=11)
    rounding_precision = models.DecimalField(max_digits=3, decimal_places=2)
    enable = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    disabled = models.DateTimeField(default=None, null=True, editable=True)

    def __str__(self):
        return "%s (%s)" % (self.name, self.symbol)


class UOMCategory(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Unit of measure category"
        verbose_name_plural = "Units of measures categories"

    def __str__(self):
        return self.name
