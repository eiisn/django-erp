from rest_framework import serializers
from product.models import AccountCategory


class AccountCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountCategory
        fields = ["id", "name", "enable"]

    def create(self, validated_data):
        validated_data["enable"] = True
        return AccountCategory.objects.create(**validated_data)
