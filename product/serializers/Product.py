from rest_framework import serializers
from product.models import Product, RawMatter, Formula, Scale, Step, Nomenclature


class ProductGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'
        depth = 2


class ProductPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        exclude = ['id']


class SalableProductGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'list_price')


class RawMatterGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = RawMatter
        fields = '__all__'
        depth = 3


class RawMatterPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = RawMatter
        exclude = ['id']


class FormulaGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Formula
        fields = '__all__'
        depth = 3


class FormulaPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Formula
        exclude = ['id']


class ScaleGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scale
        fields = '__all__'
        depth = 3


class ScalePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scale
        exclude = ['id']


class StepGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Step
        fields = '__all__'
        depth = 3


class StepPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Step
        exclude = ['id']

    def __init__(self, scale=None, *args, **kwargs):
        super(StepPOSTSerializer, self).__init__(*args, **kwargs)
        self.__scale = scale

    def create(self, validated_data):
        validated_data['scale'] = Scale.objects.get(pk=self.__scale)
        return Step.objects.create(**validated_data)


class NomenclatureGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nomenclature
        fields = '__all__'
        depth = 3


class NomenclaturePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nomenclature
        exclude = ['id']
