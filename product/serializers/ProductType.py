from rest_framework import serializers
from product.models import ProductType


class ProductTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductType
        fields = ["id", "name", "enable"]

    def create(self, validated_data):
        validated_data["enable"] = True
        return ProductType.objects.create(**validated_data)
