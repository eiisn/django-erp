"""product URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from product.views.API import *


urlpatterns = [
    path('account-category/', AccountCategoryView.as_view()),
    path('account-category/<int:pk>/', AccountCategoryView.as_view()),
    path('add-new-product/', AddProductView.as_view()),
    path('product/', ProductView.as_view()),
    path('product/<int:pk>/', ProductView.as_view()),
    path('product-salable/', SalableProductView.as_view()),
    path('raw-matter/', RawMatterView.as_view()),
    path('raw-matter/<int:pk>/', RawMatterView.as_view()),
    path('formula/', FormulaView.as_view()),
    path('formula/<int:pk>/', FormulaView.as_view()),
    path('molecule/<int:pk_formula>/', MoleculeView.as_view()),
    path('molecule/<int:pk_formula>/<int:pk>/', MoleculeView.as_view()),
    path('scale/', ScaleView.as_view()),
    path('scale/<int:pk>/', ScaleView.as_view()),
    path('step/<int:pk_scale>/', StepView.as_view()),
    path('step/<int:pk_scale>/<int:pk>/', StepView.as_view()),
    path('nomenclature/', NomenclatureView.as_view()),
    path('nomenclature/<int:pk>/', NomenclatureView.as_view()),
    path('ingredient/<int:pk_nomenclature>/', IngredientView.as_view()),
    path('ingredient/<int:pk_nomenclature>/<int:pk>/', IngredientView.as_view()),
    path('product-type/', ProductTypeView.as_view()),
    path('product-type/<int:pk>/', ProductTypeView.as_view()),
    path('unit-of-measure/', UnitOfMeasureView.as_view()),
    path('unit-of-measure/<int:pk>/', UnitOfMeasureView.as_view())
]
