from django.contrib import admin
from product.models import *

# Register your models here.
admin.site.register(Product)
admin.site.register(ProductType)
admin.site.register(Formula)
admin.site.register(RawMatter)
admin.site.register(UOMCategory)
admin.site.register(UnitOfMeasure)
admin.site.register(AccountCategory)
admin.site.register(Step)
admin.site.register(Scale)
admin.site.register(Nomenclature)
