from django.db import models
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from djmoney.models.fields import CurrencyField, CURRENCY_CHOICES
from product.models import Product


class Sale(models.Model):
    sale_date = models.DateField(blank=True, null=True)
    currency = CurrencyField(choices=CURRENCY_CHOICES)
    total_invoice = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    information = models.CharField(max_length=512)
    sale_state = models.ForeignKey("sale.SaleState", models.SET_NULL, null=True)
    customer = models.ForeignKey("customer.Customer", models.SET_NULL, null=True)
    invoice_state = models.ForeignKey("erp.InvoiceState", models.SET_NULL, null=True)
    shipment = models.ForeignKey("inventory_stock.Shipment", models.SET_NULL, null=True)

    def __str__(self):
        return "[%s] Sale to %s" % (self.sale_date, self.customer)


class SaleDetail(models.Model):
    sale = models.ForeignKey(Sale, models.CASCADE, null=True)
    product = models.ForeignKey("product.Product", models.SET_NULL, null=True)
    quantity = models.IntegerField()
    total_cost = models.DecimalField(max_digits=11, decimal_places=2)

    def __str__(self):
        return "%s x%s = %s%s" % (
            self.product.name,
            self.quantity,
            self.total_cost,
            self.sale.currency
        )


@receiver(pre_save, sender=SaleDetail)
def calculate_total_cost(sender, instance=None, **kwargs):
    try:
        instance.total_cost = instance.product.list_price * instance.quantity
    except Product.DoesNotExist:
        pass


@receiver(post_delete, sender=SaleDetail)
@receiver(post_save, sender=SaleDetail)
def calculate_total_invoice(sender, instance=None, **kwargs):
    try:
        sale = instance.sale
        sales_detail_cost = SaleDetail.objects.filter(sale__pk=sale.pk).values_list('total_cost', flat=True)
        sale.total_invoice = sum(sales_detail_cost)
        sale.save()
    except Sale.DoesNotExist:
        pass
