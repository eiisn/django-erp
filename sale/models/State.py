from django.db import models


class SaleState(models.Model):
    libel = models.CharField(max_length=255)

    def __str__(self):
        return self.libel
