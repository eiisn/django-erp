from django.contrib import admin
from sale.models import *

# Register your models here.
admin.site.register(Sale)
admin.site.register(SaleState)
admin.site.register(SaleDetail)
