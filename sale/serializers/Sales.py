from sale.models import Sale, SaleDetail
from rest_framework import serializers


class SaleGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sale
        fields = '__all__'
        depth = 3


class SalePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sale
        fields = '__all__'


class SaleDetailGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleDetail
        fields = '__all__'
        depth = 2


class SaleDetailPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleDetail
        exclude = ('total_cost',)

    def __init__(self, sale=None, *args, **kwargs):
        super(SaleDetailPOSTSerializer, self).__init__(*args, **kwargs)
        self.__sale = sale
    
    def create(self, validated_data):
        validated_data['sale'] = Sale.objects.get(pk=self.__sale)
        validated_data['total_cost'] = validated_data['product'].list_price * validated_data['quantity']
        return SaleDetail.objects.create(**validated_data)


class CRMSaleDetailPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleDetail
        exclude = ('total_cost',)
