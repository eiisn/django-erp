from rest_framework import serializers
from sale.models import SaleState


class SaleStateSerializer(serializers.ModelSerializer):

    class Meta:
        model = SaleState
        fields = '__all__'
