from utils import get_default_data
from sale.forms.Sale import SaleForm, SaleDetailForm
from django.http import JsonResponse, HttpResponseForbidden


def sale_index(request):
    if request.is_ajax():
        columns = ["", "", "Reference", "Sale Date", "Customer", "Currency", "State",
                   "Invoice State", "Shipment State", "Total Invoice", "Information"]
        data = get_default_data(request, "sale", SaleForm, None, columns, "sale/api/sale/", 2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "shipment.reference"},
            {"data": "sale_date"},
            {"data": "customer.name"},
            {"data": "currency"},
            {"data": "sale_state.libel"},
            {"data": "invoice_state.libel"},
            {"data": "shipment.state.libel"},
            {"data": "total_invoice"},
            {"data": "information"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def sale_detail_index(request, pk_sale):
    if request.is_ajax():
        columns = ["", "Product", "Unit Price", "Quantity", "Total Cost"]
        data = get_default_data(request, "sale-detail-%s" % pk_sale, SaleDetailForm, None, columns,
                                "sale/api/sale-detail/%s/" % pk_sale, 1,
                                initial_form_add={"sale": pk_sale})
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "product.list_price"},
            {"data": "quantity"},
            {"data": "total_cost"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
