import datetime

from utils.ERPView import ERPView
from utils.shipment import schedule_shipment

from rest_framework.views import APIView
from rest_framework import status, authentication, permissions
from rest_framework.response import Response

from sale.models import Sale, SaleDetail
from sale.serializers import SaleGETSerializer, SalePOSTSerializer, SaleDetailGETSerializer, \
    SaleDetailPOSTSerializer, CRMSaleDetailPOSTSerializer


class SaleView(ERPView):

    model = Sale
    post_serializer = SalePOSTSerializer
    get_serializer = SaleGETSerializer


class SaleDetailView(ERPView):

    model = SaleDetail
    post_serializer = SaleDetailPOSTSerializer
    get_serializer = SaleDetailGETSerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                sale_detail = SaleDetail.objects.get(pk=pk)
                serialize_data = SaleDetailGETSerializer(sale_detail)
            except SaleDetail.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            pk_sale = kwargs.get('pk_sale', None)
            if pk_sale:
                sale_detail = SaleDetail.objects.filter(sale__pk=pk_sale)
                serialize_data = SaleDetailGETSerializer(sale_detail, many=True)
            else:
                return Response({"data": "Need pk_sale"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        pk_sale = kwargs.get('pk_sale', None)
        if pk_sale:
            data = {"sale": pk_sale}
            serializer = SaleDetailPOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_sale"}, status=status.HTTP_404_NOT_FOUND)


class CRMSaleView(APIView):

    http_method_names = ["post"]
    authentication_classes = [authentication.TokenAuthentication, authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        sale_serializer = SalePOSTSerializer(data=request.data)
        if sale_serializer.is_valid():
            sale = sale_serializer.save()
            detail = request.data["detail"]
            for d in detail:
                d["sale"] = sale.id
            sale_detail_serializer = CRMSaleDetailPOSTSerializer(data=request.data["detail"], many=True)
            if sale_detail_serializer.is_valid():
                sale_detail_serializer.save()
                shipment = schedule_shipment(sale)
                sale.shipment = shipment
                sale.sale_date = datetime.date.today()
                sale.save()
                return Response({
                    "data": {
                        "id": shipment.id,
                        "reference": shipment.reference,
                        "planned_date": shipment.planned_date,
                        "state": shipment.state.libel
                    }
                })
        return Response({"data": "ok"})
