"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from sale.views.API import *


urlpatterns = [
    path('sale/', SaleView.as_view()),
    path('sale/<int:pk>/', SaleView.as_view()),
    path('sale-detail/<int:pk_sale>/', SaleDetailView.as_view()),
    path('sale-detail/<int:pk_sale>/<int:pk>/', SaleDetailView.as_view()),
    path('crm-sale/', CRMSaleView.as_view())
]
