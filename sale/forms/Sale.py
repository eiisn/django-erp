from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from sale.models import Sale, SaleDetail


class SaleForm(forms.ModelForm):
    sale_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )
    
    class Meta:
        model = Sale
        exclude = ('id', 'total_invoice')


class SaleDetailForm(forms.ModelForm):

    class Meta:
        model = SaleDetail
        exclude = ('id', 'total_cost')

    def __init__(self, *args, **kwargs):
        super(SaleDetailForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('sale', disabled=True),
            Field('product'),
            Field('quantity')
        )
