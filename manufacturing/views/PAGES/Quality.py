from utils import get_default_data
from manufacturing.forms.Quality import QualityForm
from django.http import JsonResponse, HttpResponseForbidden


def quality_index(request):
    if request.is_ajax():
        columns = ["", "Production Line", "Available Test", "Applied Test", "Pass Test", "Failed Test"]
        data = get_default_data(request, "quality", QualityForm, None, columns,
                                "manufacturing/api/quality/", 1)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "production_line"},
            {"data": "available_test"},
            {"data": "applied_test"},
            {"data": "pass_test"},
            {"data": "failed_test"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
