from utils import get_default_data
from manufacturing.forms.Production import ProductionForm
from django.http import JsonResponse, HttpResponseForbidden


def production_index(request):
    if request.is_ajax():
        columns = ["", "Product", "Expected Quantity", "Produced Quantity", "Start", "Expected End", "Ended", "State"]
        data = get_default_data(request, "production", ProductionForm, None, columns,
                                "manufacturing/api/production/", 1)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "expected_quantity"},
            {"data": "quantity_produced"},
            {"data": "start_date"},
            {"data": "expected_end_date"},
            {"data": "end_date"},
            {"data": "state.libel"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
