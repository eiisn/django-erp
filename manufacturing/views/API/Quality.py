from utils.ERPView import ERPView
from manufacturing.models import Quality
from manufacturing.serializers import QualityGETSerializer, QualityPOSTSerializer


class QualityView(ERPView):

    model = Quality
    post_serializer = QualityPOSTSerializer
    get_serializer = QualityGETSerializer
