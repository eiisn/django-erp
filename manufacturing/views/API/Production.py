from utils.ERPView import ERPView
from manufacturing.models import Production
from manufacturing.serializers import ProductionGETSerializer, ProductionPOSTSerializer


class ProductionView(ERPView):

    model = Production
    post_serializer = ProductionPOSTSerializer
    get_serializer = ProductionGETSerializer
