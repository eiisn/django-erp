from django import forms
from manufacturing.models import Production


class ProductionForm(forms.ModelForm):
    start_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )
    end_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )
    expected_end_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )

    class Meta:
        model = Production
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(ProductionForm, self).__init__(*args, **kwargs)
        self.fields['quantity_produced'].required = False
        self.fields['end_date'].required = False
