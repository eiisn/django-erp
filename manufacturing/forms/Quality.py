from django import forms
from manufacturing.models import Quality


class QualityForm(forms.ModelForm):

    class Meta:
        model = Quality
        exclude = ('id',)
