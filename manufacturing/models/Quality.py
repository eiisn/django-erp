from django.db import models


class Quality(models.Model):
    production_line = models.ForeignKey('Production', models.SET_NULL, null=True)
    applied_test = models.IntegerField()
    pass_test = models.IntegerField()
    failed_test = models.IntegerField()
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)

    @property
    def available_test(self):
        return self.production_line.product.productcheck_set.count()

    def __str__(self):
        return "%s | %s/%s applied %s/%s OK" % (
            self.production_line,
            self.applied_test, self.production_line.product.productcheck_set.count(),
            self.pass_test, self.applied_test
        )


class ProductCheck(models.Model):
    product = models.ForeignKey("product.Product", models.SET_NULL, null=True)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=64)

    def __str__(self):
        return "[%s] %s" % (self.product, self.name)
