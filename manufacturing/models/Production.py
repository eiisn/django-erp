from django.db import models


class Production(models.Model):
    product = models.ForeignKey("product.Product", models.SET_NULL, null=True)
    expected_quantity = models.IntegerField()
    quantity_produced = models.IntegerField(default=0, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    expected_end_date = models.DateField(blank=True, null=True)
    state = models.ForeignKey("ProductionState", models.SET_NULL, null=True)

    def __str__(self):
        return "[%s] Qts: %s for %s" % (
            self.product,
            self.expected_quantity,
            self.expected_end_date
        )


class ProductionState(models.Model):
    libel = models.CharField(max_length=64)

    def __str__(self):
        return self.libel
