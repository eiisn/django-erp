from django.contrib import admin
from manufacturing.models import *

# Register your models here.
admin.site.register(Production)
admin.site.register(ProductionState)
admin.site.register(Quality)
