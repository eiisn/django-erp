from rest_framework import serializers
from manufacturing.models import Production


class ProductionGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Production
        fields = '__all__'
        depth = 2


class ProductionPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Production
        exclude = ['id']
