from rest_framework import serializers
from manufacturing.models import Quality


class QualityGETSerializer(serializers.ModelSerializer):
    production_line = serializers.StringRelatedField()
    available_test = serializers.ReadOnlyField()

    class Meta:
        model = Quality
        fields = '__all__'
        depth = 2


class QualityPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Quality
        exclude = ['id']
