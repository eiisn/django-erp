from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_delete, post_save
from djmoney.models.fields import CurrencyField, CURRENCY_CHOICES


class Purchase(models.Model):
    purchase_date = models.DateField(blank=True, null=True)
    currency = CurrencyField(choices=CURRENCY_CHOICES)
    total_invoice = models.DecimalField(max_digits=11, decimal_places=2, default=0)
    information = models.CharField(max_length=512)
    purchase_state = models.ForeignKey("purchase.PurchaseState", models.SET_NULL, null=True)
    invoice_state = models.ForeignKey("erp.InvoiceState", models.SET_NULL, null=True)
    shipment = models.ForeignKey("inventory_stock.Shipment", models.SET_NULL, null=True)
    customer = models.ForeignKey("customer.Customer", models.SET_NULL, null=True)

    def __str__(self):
        return "[%s] Purchase to %s" % (self.purchase_date, self.customer)


class PurchaseDetail(models.Model):
    purchase = models.ForeignKey(Purchase, models.CASCADE, null=True)
    product = models.ForeignKey("product.Product", models.SET_NULL, null=True)
    quantity = models.IntegerField()
    total_cost = models.DecimalField(max_digits=11, decimal_places=2)

    def __str__(self):
        return "%s x%s = %s%s" % (
            self.product.name,
            self.quantity,
            self.total_cost,
            self.purchase.currency
        )


@receiver(pre_save, sender=PurchaseDetail)
def calculate_total_cost(sender, instance=None, **kwargs):
    instance.total_cose = instance.product.list_price * instance.quantity


@receiver(post_delete, sender=PurchaseDetail)
@receiver(post_save, sender=PurchaseDetail)
def calculate_total_invoice(sender, instance=None, **kwargs):
    purchase = instance.purchase
    purchase_detail_cost = PurchaseDetail.objects.filter(purchase__pk=purchase.pk).values_list('total_cost', flat=True)
    purchase.total_invoice = sum(purchase_detail_cost)
    purchase.save()
