from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from purchase.models import Purchase, PurchaseDetail


class PurchaseForm(forms.ModelForm):
    purchase_date = forms.DateField(
        widget=forms.TextInput(
            attrs={
                'type': 'date',
            }
        )
    )

    class Meta:
        model = Purchase
        exclude = ('id', 'total_invoice')
        
    
class PurchaseDetailForm(forms.ModelForm):
    
    class Meta:
        model = PurchaseDetail
        exclude = ('id', 'total_cost')
    
    def __init__(self, *args, **kwargs):
        super(PurchaseDetailForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('purchase', disabled=True),
            Field('product'),
            Field('quantity')
        )
