from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden
from purchase.forms.Purchase import PurchaseForm, PurchaseDetailForm


def purchase_index(request):
    if request.is_ajax():
        columns = ["", "", "Reference", "Purchase Date", "Customer", "Currency", "State",
                   "Invoice State", "Shipment State", "Total Invoice", "Information"]
        data = get_default_data(request, "purchase", PurchaseForm, None, columns, "purchase/api/purchase/", 2,
                                script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "shipment.reference"},
            {"data": "purchase_date"},
            {"data": "customer.name"},
            {"data": "currency"},
            {"data": "purchase_state.libel"},
            {"data": "invoice_state.libel"},
            {"data": "shipment.state.libel"},
            {"data": "total_invoice"},
            {"data": "information"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def purchase_detail_index(request, pk_purchase):
    if request.is_ajax():
        columns = ["", "Product", "Unit Price", "Quantity", "Total Cost"]
        data = get_default_data(request, "purchase-detail-%s" % pk_purchase, PurchaseDetailForm, None, columns,
                                "purchase/api/purchase-detail/%s/" % pk_purchase, 1,
                                initial_form_add={"purchase": pk_purchase})
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "product.name"},
            {"data": "product.list_price"},
            {"data": "quantity"},
            {"data": "total_cost"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
