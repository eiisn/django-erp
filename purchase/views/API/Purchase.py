from utils.ERPView import ERPView
from rest_framework import status
from rest_framework.response import Response
from purchase.models import Purchase, PurchaseDetail
from purchase.serializers import PurchaseGETSerializer, PurchasePOSTSerializer, \
    PurchaseDetailGETSerializer, PurchaseDetailPOSTSerializer


class PurchaseView(ERPView):

    model = Purchase
    post_serializer = PurchasePOSTSerializer
    get_serializer = PurchaseGETSerializer


class PurchaseDetailView(ERPView):

    model = PurchaseDetail
    post_serializer = PurchaseDetailPOSTSerializer
    get_serializer = PurchaseDetailGETSerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                purchase_detail = PurchaseDetail.objects.get(pk=pk)
                serialize_data = PurchaseDetailGETSerializer(purchase_detail)
            except PurchaseDetail.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            pk_purchase = kwargs.get('pk_purchase', None)
            if pk_purchase:
                purchase_detail = PurchaseDetail.objects.filter(purchase__pk=pk_purchase)
                serialize_data = PurchaseDetailGETSerializer(purchase_detail, many=True)
            else:
                return Response({"data": "Need pk_purchase"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        pk_purchase = kwargs.get('pk_purchase', None)
        if pk_purchase:
            data = {"purchase": pk_purchase}
            serializer = PurchaseDetailPOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_purchase"}, status=status.HTTP_404_NOT_FOUND)
