from rest_framework import serializers
from purchase.models import PurchaseState


class PurchaseStateSerializer(serializers.ModelSerializer):

    class Meta:
        model = PurchaseState
        fields = '__all__'
