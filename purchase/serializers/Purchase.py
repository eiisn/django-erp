from purchase.models import Purchase, PurchaseDetail
from rest_framework import serializers


class PurchaseGETSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'
        depth = 3


class PurchasePOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'


class PurchaseDetailGETSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseDetail
        fields = '__all__'
        depth = 2


class PurchaseDetailPOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseDetail
        exclude = ('total_cost',)

    def __init__(self, purchase=None, *args, **kwargs):
        super(PurchaseDetailPOSTSerializer, self).__init__(*args, **kwargs)
        self.__purchase = purchase

    def create(self, validated_data):
        validated_data['purchase'] = Purchase.objects.get(pk=self.__purchase)
        validated_data['total_cost'] = validated_data['product'].list_price * validated_data['quantity']
        return PurchaseDetail.objects.create(**validated_data)
